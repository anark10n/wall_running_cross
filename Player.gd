extends KinematicBody

var mouse_sensitivity = 0.3


var speed = 15
var accel_type = {"default": 40, "air": 1}
onready var accel = accel_type["default"]
var gravity = 20
var jump = 10


var cam_accel = 40
var mouse_sense = 0.1
var snap


var direction = Vector3()
var velocity = Vector3()
var g_vector = Vector3()
var movement = Vector3()


onready var head = $Head
onready var camera = $Head/Camera
onready var ground_check = $Groundcast
onready var l_wall_check = $Wallcast/LWallcast
onready var r_wall_check = $Wallcast/RWallcast


func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)


func _input(event):
	if event is InputEventMouseMotion:
		rotate_y(deg2rad(-event.relative.x * mouse_sense))
		head.rotate_x(deg2rad(-event.relative.y * mouse_sense))
		head.rotation.x = clamp(head.rotation.x, deg2rad(-89), deg2rad(89))


func _process(delta):
	if Engine.get_frames_per_second() > Engine.iterations_per_second:
		camera.set_as_toplevel(true)
		camera.global_transform.origin = camera.global_transform.origin.linear_interpolate(head.global_transform.orgin, cam_accel * delta)
		camera.rotation.y = rotation.y
		camera.rotation.x = head.rotation.x
	else:
		camera.set_as_toplevel(false)
		camera.global_transform = head.global_transform


func _physics_process(delta):
	direction = Vector3.ZERO
	
	
	var h_rot = global_transform.basis.get_euler().y
	var fnb = Input.get_action_strength("move_backward") - Input.get_action_strength("move_forward")
	var lnr = Input.get_action_strength("move_right") - Input.get_action_strength("move_left")
	direction = Vector3(lnr, 0, fnb).rotated(Vector3.UP, h_rot).normalized()
	
	
	if is_on_floor():
		snap = -get_floor_normal()
		accel = accel_type["default"]
		g_vector = Vector3.ZERO
	else:
		snap = Vector3.DOWN
		accel = accel_type["air"]
		g_vector += Vector3.DOWN * gravity * delta
	
	
	if Input.is_action_just_pressed("jump") and is_on_floor():
		snap = Vector3.ZERO
		g_vector = Vector3.UP * jump
	
	if Input.is_action_pressed("wall_run") and wall_check() and not is_on_floor():
		g_vector = Vector3.ZERO
		if l_wall_check.is_colliding():
			var w = l_wall_check.get_collision_point()
			direction = Vector3.UP.cross(-l_wall_check.get_collision_normal()).normalized()
		elif r_wall_check.is_colliding():
			var w = r_wall_check.get_collision_point()
			direction = Vector3.UP.cross(r_wall_check.get_collision_normal()).normalized()
		else:
			direction = Vector3(lnr, 0, fnb).rotated(Vector3.UP, h_rot).normalized()
	
	
	velocity = velocity.linear_interpolate(direction * speed, accel * delta)
	movement = velocity + g_vector
	
	
	move_and_slide_with_snap(movement, snap, Vector3.UP)


func wall_check():
	if r_wall_check.is_colliding() or l_wall_check.is_colliding():
		return true
	else:
		return false
